package Model;

import java.util.Iterator;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Server implements Runnable {

    private LinkedBlockingQueue<Task> clients;
    private AtomicInteger period;
    private int nbOfClients;
    private int nbMaxOfClients = 1000;
    private volatile boolean flag = false;
    private double waitingTime = 0;

    public Server(){
        clients = new LinkedBlockingQueue<Task>();
        period = new AtomicInteger(0);
        nbOfClients = 0;
    }

    public void stopThread(){
        flag = false;
    }

    public void startThread(){
        flag = true;
        Thread t = new Thread(this);
        t.start();
    }

    public AtomicInteger getPeriod(){
        return this.period;
    }

    public void incrementClientsWaiting(){

        Iterator<Task> it=clients.iterator();

        while(it.hasNext()){
            Task task=it.next();
            task.incrementFinishTime();
        }

    }

    public void addClient(Task t){

        if(nbOfClients < nbMaxOfClients){
            clients.add(t);
            nbOfClients++;
            period.addAndGet(t.getProcTime());

        }
        if(!flag){
            this.startThread();
        }
    }

    public Task removeClient(){

        Task first = this.clients.peek();
        this.clients.poll();
        return first;
    }

    public LinkedBlockingQueue<Task> getClients(){
        return this.clients;
    }

    public int getProcTime(){
        int time = 0;
        for(Task t: this.clients){
            time += t.getProcTime();
        }
        return time;
    }

    public double getWaitingTime(){
        return this.waitingTime;
    }

    public void run() {

        while(flag){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if(clients.isEmpty()){
                this.stopThread();
            }
            else{
                incrementClientsWaiting();
                period.decrementAndGet();
                clients.peek().decremeProcTim();
                if(clients.peek().getProcTime() == 0){
                    this.waitingTime += clients.peek().getFinishTime();
                    this.removeClient();
                }
            }
        }
    }
}
