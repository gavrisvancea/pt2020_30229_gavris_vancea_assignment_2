package Model;

public class Task implements Comparable{

    private int id;
    private int arrvTime;
    private int procTime;
    private int finishTime;

    public Task(int i, int a, int p){
        this.id = i;
        this.arrvTime = a;
        this.procTime = p;
        this.finishTime = 0;
    }

    public int getProcTime() {
        return this.procTime;
    }

    public void decremeProcTim(){
        this.procTime -=1;
    }

    public int getArrivalTime(){
        return arrvTime;
    }

    public void incrementFinishTime(){
        this.finishTime++;
    }

    public int getFinishTime(){
        return this.finishTime;
    }

    public String toString(){
        String s = "";

        return s + " (" + id + ", " + arrvTime + ", " + procTime + ") " + finishTime+" ";
    }

    public int compareTo(Object o) {
        Task t = (Task)o;

        return this.arrvTime - t.getArrivalTime();
    }
}
