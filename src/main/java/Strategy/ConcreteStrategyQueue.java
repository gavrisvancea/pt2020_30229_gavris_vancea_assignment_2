package Strategy;

import Model.Server;
import Model.Task;

import java.util.*;

public class ConcreteStrategyQueue implements Strategy{

    public void addClient(ArrayList<Server> servers, Task client){
        int minClients = 9999;

        for(Server i:servers){
            if(i.getClients().size() <= minClients){
                minClients = i.getClients().size();
            }
        }

        for(Server i: servers){
            if(i.getClients().size() == minClients){
                i.addClient(client);
                break;
            }
        }
    }
}
